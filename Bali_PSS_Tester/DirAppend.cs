﻿using System;
using System.IO;
using System.Collections.Generic;

public class DirAppend
{
	public DirAppend()
	{
	}
    
    public static void Append(List<float> messagelist)
    {
        if (messagelist == null)
            throw new ArgumentNullException("messagelist"); 

        var logMessage = new string[messagelist.Count];
        int i = 0;
        foreach(float item in messagelist)
        {      
            logMessage[i] = item.ToString("F3");
            i++;
        }
        
        using (StreamWriter w= File.AppendText(@"c:\psuBali\pssBali.cvs"))
        {
            Log( String.Join(",",logMessage), w);
        }
    }

    private static void Log(string logMessage, TextWriter w)
    {
        w.Write("\r\n{0} {1} {2} {3} {4}",DateTime.Now.ToLongTimeString(), ",", DateTime.Now.ToShortDateString(),",",logMessage);
    }
    
}
