﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;
using Bali_PSS_Tester;

namespace Bali_PSS_Tester
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Program to initialize Bali PDU and PSS and perform start/stop tests
        /// Data to be collected at each cycle is cycle #, voltages and temperture.
        /// Data is to be logged to a file each cycle.
        /// Oct. 10, 2016 Brian Tremaine
        /// </summary>
        /// 

        enum testState
        {
            STOPPED = 0,
            RUNNING,
            PAUSED
        }

        private int durationTimerTics;
        private DateTime now;
        private DateTime startDate;
        private TimeSpan elapsed;
        private int testCycles;
        private testState runState = testState.STOPPED;

        private static int DEBUG = 1;               // normally 1 but set to 6 for debugging timer
        private static float LIMIT48 = 40.0f;
        private static float LIMIT24 = 20.0f;
        private static float TEMP_MAX_HIGH = 110.0f;
        private static float TEMP_MAX_PDU = 60.0f;

        private PSS_Test pss = new PSS_Test();

        public Form1()
        {
            InitializeComponent(); 
            pss.ipAddress=  "10.10.10.6"; // static ip address
            pss.initPDU();
        }
        
        private void start_button_Click(object sender, EventArgs e)
        {
            switch (runState)
            {
                case testState.STOPPED:
                    // Setup timer
                    durationTimerTics = getDurationTicks();
                    timer1.Interval = durationTimerTics;
                    startDate = DateTime.Now;
                    runState = testState.RUNNING;
                    buttonStart.BackColor = Color.Red;
                    buttonStart.Text = "running...";
                    // clear display & test cycles
                    // update elapsed time
                    textBox5.Text = "0";
                    // update # cycles
                    testCycles = 0;
                    textBoxCycles.Text = testCycles.ToString();
                    timer1.Start();
                    // get first sample
                    pss.SampleEvent(false);
                    break;
                case testState.PAUSED:
                    runState = testState.RUNNING;
                    buttonStart.BackColor = Color.Red;
                    buttonStart.Text = "running...";
                    timer1.Start();
                    break;
                case testState.RUNNING:
                    runState = testState.PAUSED;
                    buttonStart.BackColor = Color.LightYellow;
                    buttonStart.Text = "paused...";
                    timer1.Stop();
                    break;
            }
        }  

        private int getDurationTicks()
        {
            durationTimerTics = (int)( (numericUpDown1.Value) * 60 * 1000 / DEBUG);          // msec --- check size ---
            durationTimerTics = durationTimerTics < 1000 ? 1000 : durationTimerTics; // min value

            return durationTimerTics;
        }

        private void buttonEnd_Click(object sender, EventArgs e)
        {
            DialogResult result1 = MessageBox.Show("Do you want the test to terminate?","Caution!", MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                pss.StopTest(false); // argument controls mains power
                Close();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(sender == timer1)
            {
                // process event here
                var switch_ac = (testCycles % (int) numericUpDown2.Value) == 0? true:false ;
                pss.SampleEvent((bool) switch_ac );      // does data acquisition

                // update display
                float TA = pss.TA;
                textBoxTamb.Text = TA.ToString("F3");

                float T1 = pss.T1 ;
                textBoxTemp1.Text = T1.ToString("F3");

                float T2 = pss.T2 ;
                textBoxTemp2.Text = T2.ToString("F3");

                float T3 = pss.T3 ;
                textBoxTemp3.Text = T3.ToString("F3");

                float TPD = pss.TPD;
                textBoxPDUtemp.Text = TPD.ToString("F3");

                float Vin48 = pss.V48;
                textBox48VDC.Text = Vin48.ToString("F3");

                float Vin24 = pss.V24;
                textBox24VDC.Text = Vin24.ToString("F3");

                float mod1 = pss.PSU1 ? 1.0f : 0.0f;
                float mod2 = pss.PSU2 ? 1.0f : 0.0f;
                float mod3 = pss.PSU3 ? 1.0f : 0.0f;
                float mod4 = pss.PSU4 ? 1.0f : 0.0f;
                float mod5 = pss.PSU5 ? 1.0f : 0.0f;
                float mod6 = pss.PSU6 ? 1.0f : 0.0f;

                // update module status
                pictureBoxPSU1.BackColor = pss.PSU1 ? Color.Green : Color.Red;
                pictureBoxPSU2.BackColor = pss.PSU2 ? Color.Green : Color.Red;
                pictureBoxPSU3.BackColor = pss.PSU3 ? Color.Green : Color.Red;
                pictureBoxPSU4.BackColor = pss.PSU4 ? Color.Green : Color.Red;
                pictureBoxPSU5.BackColor = pss.PSU5 ? Color.Green : Color.Red;
                pictureBoxPSU6.BackColor = pss.PSU6 ? Color.Green : Color.Red;

                // update elapsed time (in hours)
                now = DateTime.Now;
                elapsed = now.Subtract(startDate);
                var duration = (float) elapsed.TotalHours;
                textBox5.Text= duration.ToString("F3");
                // update # cycles
                testCycles ++;
                textBoxCycles.Text = testCycles.ToString();

                // log data
                var messagelist = new List<float> { duration, Vin48, Vin24, TA, T1, T2, T3, mod1, mod2, mod3, mod4, mod5, mod6 };
                if(messagelist==null)
                    throw new ArgumentNullException("messagelist");
                DirAppend.Append(messagelist);

                // periodically send report
                if (testCycles % 36 == 0)
                {
                    SendReport("Update");
                }

                // check for errors
                if (checkErrors())
                {
                    timer1.Stop();
                    pss.StopTest(false); // argument controls mains power
                    DialogResult result1 = MessageBox.Show("Test aborted due to limit error","Caution", MessageBoxButtons.OK);
                    Close();
                }
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            // changing test duration
            // only respond to ...\n

            DialogResult result1;

            if (runState == testState.RUNNING)
            { 
                result1 = MessageBox.Show("Test is running, will stop timer & pause", "Caution!", MessageBoxButtons.OKCancel);
                if (result1 == DialogResult.OK)
                {
                    timer1.Stop();
                    buttonStart.Text = "pause...";
                    buttonStart.BackColor = Color.LightYellow;
                    runState = testState.PAUSED;
                }
            }
            else
            {
                // get new timer value
                durationTimerTics = getDurationTicks();
            }
             
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            // stop test
            timer1.Stop();
            runState = testState.STOPPED;
            buttonStart.BackColor = Color.LightGreen;
            buttonStart.Text = "Start";

            // ... do PSS stop here..
            pss.StopTest(false);

            // need some delay?
            // update module status
            pictureBoxPSU1.BackColor = pss.PSU1 ? Color.Green : Color.Red;
            pictureBoxPSU2.BackColor = pss.PSU2 ? Color.Green : Color.Red;
            pictureBoxPSU3.BackColor = pss.PSU3 ? Color.Green : Color.Red;
            pictureBoxPSU4.BackColor = pss.PSU4 ? Color.Green : Color.Red;
            pictureBoxPSU5.BackColor = pss.PSU5 ? Color.Green : Color.Red;
            pictureBoxPSU6.BackColor = pss.PSU6 ? Color.Green : Color.Red;
        }

        private bool checkErrors()
        {
            // check for errors 
            // eventually put limits in an ini file in lieu of defaults
            // send email & shut down in case of gross error

            bool error = false;

            error = pss.V48 < LIMIT48 ? true : error;
            error = pss.V24 < LIMIT24 ? true : error;
            error = pss.T1 > TEMP_MAX_HIGH ? true : error;
            error = pss.T2 > TEMP_MAX_HIGH ? true : error;
            error = pss.T3 > TEMP_MAX_HIGH ? true : error;
            error = pss.TPD > TEMP_MAX_PDU ? true : error;

            string subject;

            if (error)
            {
                pss.StopTest(false);
                subject = "Error!!! ...." + Environment.NewLine;

                SendReport(subject); 
            }

            return error;
        }

        private bool SendReport(string subject)
        {
            uint status = 0;
            if (pss.PSU6)
                status = 1 << 5;
            if (pss.PSU5)
                status = status + (1 << 4);
            if (pss.PSU4)
                status = status + (1 << 3);
            if (pss.PSU3)
                status = status + (1 << 2);
            if (pss.PSU2)
                status = status + (1 << 1);
            if (pss.PSU1)
                status = status + 1;

            // send message
            string message = Environment.NewLine
                                + testCycles.ToString("D") + Environment.NewLine
                                + pss.V48.ToString("F1") + Environment.NewLine
                                + pss.V24.ToString("F1") + Environment.NewLine
                                + pss.T1.ToString("F1") + Environment.NewLine
                                + pss.T2.ToString("F1") + Environment.NewLine
                                + pss.T3.ToString("F1") + Environment.NewLine
                                + pss.TPD.ToString("F1") + Environment.NewLine
                                + status.ToString("X") + Environment.NewLine;

            try
            {
                SendMessaging.SendSmtpMessage(subject + message);  // need smtp client

                return true;
            }
            catch
            {
                throw;
            }

        }


    }
}
