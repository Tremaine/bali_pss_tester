﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections.Generic;
using LabJack.LabJackUD;
using PDU;

// Define connections to Labjack UE9
// DIO
// SSR1 for AC1: FIO0, active high true 4.5V/5mA
// SSR2 for AC2: FIO1, active high true
// -------------------
// Temp Sense 1: AIN0, 
// Temp Sense 2: AIN1, 
// Temp Sense 3: AIN2, 
// -------------------
// Connection to PDU
// Ethernet:
//
//
//
//


namespace Bali_PSS_Tester
{
    public class PSS_Test
    {

        public PSS_Test()
        {
            initLJUE9();
        }
        
        public float T1 { get; set; }
        public float T2 { get; set; }
        public float T3 { get; set; }
        public float TA { get; set; }
        public float TPD { get; set; }
        public float V48 { get; set; }
        public float V24 { get; set; }
        public bool PSU1 { get; set; }
        public bool PSU2 { get; set; }
        public bool PSU3 { get; set; }
        public bool PSU4 { get; set; }
        public bool PSU5 { get; set; }
        public bool PSU6 { get; set; }

        public string ipAddress = null;

        private UE9 ue9;
        private PDU.PDU p;


        private enum SSRunit {
            ONE = 0,
            TWO = 1
        }

        private enum TEMPunit
        {
            ONE = 0,
            TWO = 1,
            THREE = 2,
            INTERNAL
        }

        private enum DIO
        {
            FIO0 = 0,
            FIO1 = 1,
            FIO2 = 2,
            FIO3 = 3
        }

        private enum AIO
        {
            AIN0 = 0,
            AIN1 = 1,
            AIN2 = 2,
            AIN3 = 3,
            AIN4 = 4,
            INTERNAL = 133
        }

        private enum PduChan
        {
            V48Chan = 0,
            V24Chan = 1
        }

        public void initLJUE9()
        {
            // initialize labjack & PDU
            // Open UE9
            try
            {
                ue9 = new UE9(LJUD.CONNECTION.USB, "0", true); // Connection through USB

                if(false) // debug
                {
                    // measure temperature (uses ADC)
                    T1 = ReadTemp(TEMPunit.ONE);
                    T2 = ReadTemp(TEMPunit.TWO);
                    T3 = ReadTemp(TEMPunit.THREE);
                    TA = ReadTemp(TEMPunit.INTERNAL);
                    SSR(SSRunit.ONE, true);
                    System.Threading.Thread.Sleep(5000);
                    SSR(SSRunit.TWO, true);
                    System.Threading.Thread.Sleep(5000);
                }
            }
            catch (LabJackUDException e)
            {
                showErrorMessage(e);
            }
        }

        public void initPDU()
        {
            try
            {
                // use static IP address for PDU
                p = new PDU.PDU("Main PDU", ipAddress, 9600, 23, 1000, PDU.BoardVersion.Spin2);
            }
            catch (NotImplementedException )
            {
                throw;
            }
        }

        public void showErrorMessage(LabJackUDException e)
        {
            MessageBox.Show("Error: " + e.ToString());
        }

        public void StopTest(bool mains)
        {
            // turn off PSS
            // ...
            // turn off AC
            if (mains)
            {
                SSR(SSRunit.ONE, false);
                SSR(SSRunit.TWO, false);
            }
        }

        public void SampleEvent(bool flag)
        {
            // ensure AC is ON for sample
            SSR(SSRunit.ONE, true);
            // need short delay for inrush ?
            System.Threading.Thread.Sleep(100);
            SSR(SSRunit.TWO, true);


            // measure temperature (uses ADC)
            T1 = ReadTemp(TEMPunit.ONE);
            T2 = ReadTemp(TEMPunit.TWO);
            T3 = ReadTemp(TEMPunit.THREE);
            TA = ReadTemp(TEMPunit.INTERNAL);

            TPD = ReadPduTemperature();

            // measure voltages
            V48 = ReadPduVoltage(PduChan.V48Chan);
            V24 = ReadPduVoltage(PduChan.V24Chan);  // "supported ??"

            // get PSU status, updates PSU1:PSU6
            getPsuStatus();

            // turn off loads
            LoadEnable(false);
            System.Threading.Thread.Sleep(2000);

            // turn OFF AC then back ON
            if (flag)
            {
                SSR(SSRunit.ONE, false);
                SSR(SSRunit.TWO, false);

                System.Threading.Thread.Sleep(10000); // let caps discharge 10 sec

                SSR(SSRunit.ONE, true);
                // need short delay for inrush ?
                System.Threading.Thread.Sleep(2000);
                SSR(SSRunit.TWO, true);
                System.Threading.Thread.Sleep(5000);
            }

            // reconnectPDU --- may not be working
                //  System.Threading.Thread.Sleep(20000);
                //  p.reconnectPDU();

            // turn on loads
            LoadEnable(true);

        }

        private void SSR(SSRunit unit, bool state)
        {
            // change state of device 'unit' to 'state'
            int val = state? 1 : 0;
            switch (unit)
            {
                case SSRunit.ONE:
                    LJUD.eDO(ue9.ljhandle, (int) DIO.FIO0, val);
                    break;
                case SSRunit.TWO:
                    LJUD.eDO(ue9.ljhandle, (int) DIO.FIO1, val);
                    break;
            }
        }

        private float ReadTemp(TEMPunit unit)
        {
            // read LJ Temp Sensor & convert to deg C (float)
            float temp = 0;

            int channel = 0;
            int range = (int)LJUD.RANGES.UNI5V;
            int resolution = 17;
            int settling = 0;
            int binary = 0;
            double volts = 0;

            switch (unit)
            {
                case TEMPunit.ONE:
                    channel = (int)AIO.AIN0 ;
                    break;
                case TEMPunit.TWO:
                    channel = (int)AIO.AIN1;
                    break;
                case TEMPunit.THREE:
                    channel = (int)AIO.AIN2;
                    break;
                case TEMPunit.INTERNAL:
                    channel = (int)AIO.INTERNAL;
                    break;
            }

            LJUD.eAIN(ue9.ljhandle, (int)channel, 0, ref volts, range, resolution, settling, binary);

            // convert ADC to deg C value
            if (unit == TEMPunit.INTERNAL)
                temp = (float)volts * 1.00f - 273.15f;
            else
                temp = (float)(55.56f * volts + 255.37f-273.15f);

            return temp;
        }

        private float ReadPduVoltage(PduChan chan)
        {
            // read channels in PDU
            float volts = 0;

            switch(chan)
            {
                case PduChan.V48Chan:
                    
                    volts = (float)p.readVoltage();
                    break;
                case PduChan.V24Chan:
                    volts = (float)p.readVoltage24(); // "not supported?";
                    break;
            }

            return volts;
        }

        private float ReadPduTemperature()
        {
            return (float)p.readTemperature();
        }

        private void LoadEnable(bool enable)
        {
            // enable PDU output loads
            var chanlist = new List<int> { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'};

            foreach (char item in chanlist)
            {
                p.enableColumn(item, enable);
            }
        }

        private byte getPsuStatus()
        {
            // update PSU status
            byte status = 0;

            status = p.getDCInputStatus();

            PSU1 = (status & 0x01) == 0 ? true : false;
            PSU2 = (status>>1 & 0x01) == 0 ? true : false;
            PSU3 = (status>>2 & 0x01) == 0 ? true : false;
            PSU4 = (status>>3 & 0x01) == 0 ? true : false;
            PSU5 = (status>>4 & 0x01) == 0 ? true : false;
            PSU6 = (status>>5 & 0x01) == 0 ? true : false;

            return status;
        }

    }
}
