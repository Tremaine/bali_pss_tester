﻿namespace Bali_PSS_Tester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonEnd = new System.Windows.Forms.Button();
            this.textBoxCycles = new System.Windows.Forms.TextBox();
            this.textBox48VDC = new System.Windows.Forms.TextBox();
            this.textBox24VDC = new System.Windows.Forms.TextBox();
            this.textBoxTemp1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button_Stop = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxTemp2 = new System.Windows.Forms.TextBox();
            this.textBoxTemp3 = new System.Windows.Forms.TextBox();
            this.textBoxPDUtemp = new System.Windows.Forms.TextBox();
            this.textBoxTamb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.pictureBoxPSU1 = new System.Windows.Forms.PictureBox();
            this.labelMod1 = new System.Windows.Forms.Label();
            this.pictureBoxPSU2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxPSU3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxPSU4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxPSU5 = new System.Windows.Forms.PictureBox();
            this.pictureBoxPSU6 = new System.Windows.Forms.PictureBox();
            this.labelMod2 = new System.Windows.Forms.Label();
            this.labelMod3 = new System.Windows.Forms.Label();
            this.labelMod4 = new System.Windows.Forms.Label();
            this.labelMod5 = new System.Windows.Forms.Label();
            this.labelMod6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(29, 29);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(90, 23);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.start_button_Click);
            // 
            // buttonEnd
            // 
            this.buttonEnd.Location = new System.Drawing.Point(512, 286);
            this.buttonEnd.Name = "buttonEnd";
            this.buttonEnd.Size = new System.Drawing.Size(75, 23);
            this.buttonEnd.TabIndex = 1;
            this.buttonEnd.Text = "End";
            this.buttonEnd.UseVisualStyleBackColor = true;
            this.buttonEnd.Click += new System.EventHandler(this.buttonEnd_Click);
            // 
            // textBoxCycles
            // 
            this.textBoxCycles.Location = new System.Drawing.Point(29, 234);
            this.textBoxCycles.Name = "textBoxCycles";
            this.textBoxCycles.ReadOnly = true;
            this.textBoxCycles.Size = new System.Drawing.Size(80, 20);
            this.textBoxCycles.TabIndex = 2;
            // 
            // textBox48VDC
            // 
            this.textBox48VDC.Location = new System.Drawing.Point(149, 233);
            this.textBox48VDC.Name = "textBox48VDC";
            this.textBox48VDC.ReadOnly = true;
            this.textBox48VDC.Size = new System.Drawing.Size(80, 20);
            this.textBox48VDC.TabIndex = 3;
            // 
            // textBox24VDC
            // 
            this.textBox24VDC.Location = new System.Drawing.Point(269, 233);
            this.textBox24VDC.Name = "textBox24VDC";
            this.textBox24VDC.ReadOnly = true;
            this.textBox24VDC.Size = new System.Drawing.Size(80, 20);
            this.textBox24VDC.TabIndex = 4;
            // 
            // textBoxTemp1
            // 
            this.textBoxTemp1.Location = new System.Drawing.Point(512, 185);
            this.textBoxTemp1.Name = "textBoxTemp1";
            this.textBoxTemp1.ReadOnly = true;
            this.textBoxTemp1.Size = new System.Drawing.Size(80, 20);
            this.textBoxTemp1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 208);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Elapsed Cycles";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(146, 208);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "48VDC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(266, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "24VDC";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(509, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Load 1 Temp, C";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(29, 289);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(80, 20);
            this.textBox5.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 273);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Elapsed Hrs";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(125, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Run/Pause";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(296, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "min/sample";
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button_Stop
            // 
            this.button_Stop.Location = new System.Drawing.Point(29, 76);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(90, 23);
            this.button_Stop.TabIndex = 15;
            this.button_Stop.Text = "Stop";
            this.button_Stop.UseVisualStyleBackColor = true;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(369, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(129, 43);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxTemp2
            // 
            this.textBoxTemp2.Location = new System.Drawing.Point(512, 135);
            this.textBoxTemp2.Name = "textBoxTemp2";
            this.textBoxTemp2.ReadOnly = true;
            this.textBoxTemp2.Size = new System.Drawing.Size(80, 20);
            this.textBoxTemp2.TabIndex = 17;
            // 
            // textBoxTemp3
            // 
            this.textBoxTemp3.Location = new System.Drawing.Point(512, 85);
            this.textBoxTemp3.Name = "textBoxTemp3";
            this.textBoxTemp3.ReadOnly = true;
            this.textBoxTemp3.Size = new System.Drawing.Size(80, 20);
            this.textBoxTemp3.TabIndex = 18;
            // 
            // textBoxPDUtemp
            // 
            this.textBoxPDUtemp.Location = new System.Drawing.Point(512, 235);
            this.textBoxPDUtemp.Name = "textBoxPDUtemp";
            this.textBoxPDUtemp.ReadOnly = true;
            this.textBoxPDUtemp.Size = new System.Drawing.Size(80, 20);
            this.textBoxPDUtemp.TabIndex = 35;
            // 
            // textBoxTamb
            // 
            this.textBoxTamb.Location = new System.Drawing.Point(512, 35);
            this.textBoxTamb.Name = "textBoxTamb";
            this.textBoxTamb.ReadOnly = true;
            this.textBoxTamb.Size = new System.Drawing.Size(80, 20);
            this.textBoxTamb.TabIndex = 37;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(509, 119);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Load 2 Temp. C";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(509, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Load 3 Temp. C";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "psuBali*.cvs";
            this.openFileDialog1.InitialDirectory = "c:\\psuBali";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.AllowDrop = true;
            this.numericUpDown1.Location = new System.Drawing.Point(213, 32);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            1440,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(80, 20);
            this.numericUpDown1.TabIndex = 21;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // pictureBoxPSU1
            // 
            this.pictureBoxPSU1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPSU1.Location = new System.Drawing.Point(446, 65);
            this.pictureBoxPSU1.Name = "pictureBoxPSU1";
            this.pictureBoxPSU1.Size = new System.Drawing.Size(20, 20);
            this.pictureBoxPSU1.TabIndex = 22;
            this.pictureBoxPSU1.TabStop = false;
            // 
            // labelMod1
            // 
            this.labelMod1.AutoSize = true;
            this.labelMod1.Location = new System.Drawing.Point(384, 72);
            this.labelMod1.Name = "labelMod1";
            this.labelMod1.Size = new System.Drawing.Size(51, 13);
            this.labelMod1.TabIndex = 23;
            this.labelMod1.Text = "Module 1";
            // 
            // pictureBoxPSU2
            // 
            this.pictureBoxPSU2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPSU2.Location = new System.Drawing.Point(446, 95);
            this.pictureBoxPSU2.Name = "pictureBoxPSU2";
            this.pictureBoxPSU2.Size = new System.Drawing.Size(20, 20);
            this.pictureBoxPSU2.TabIndex = 24;
            this.pictureBoxPSU2.TabStop = false;
            // 
            // pictureBoxPSU3
            // 
            this.pictureBoxPSU3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPSU3.Location = new System.Drawing.Point(446, 125);
            this.pictureBoxPSU3.Name = "pictureBoxPSU3";
            this.pictureBoxPSU3.Size = new System.Drawing.Size(20, 20);
            this.pictureBoxPSU3.TabIndex = 25;
            this.pictureBoxPSU3.TabStop = false;
            // 
            // pictureBoxPSU4
            // 
            this.pictureBoxPSU4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPSU4.Location = new System.Drawing.Point(446, 155);
            this.pictureBoxPSU4.Name = "pictureBoxPSU4";
            this.pictureBoxPSU4.Size = new System.Drawing.Size(20, 20);
            this.pictureBoxPSU4.TabIndex = 26;
            this.pictureBoxPSU4.TabStop = false;
            // 
            // pictureBoxPSU5
            // 
            this.pictureBoxPSU5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPSU5.Location = new System.Drawing.Point(446, 185);
            this.pictureBoxPSU5.Name = "pictureBoxPSU5";
            this.pictureBoxPSU5.Size = new System.Drawing.Size(20, 20);
            this.pictureBoxPSU5.TabIndex = 27;
            this.pictureBoxPSU5.TabStop = false;
            // 
            // pictureBoxPSU6
            // 
            this.pictureBoxPSU6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPSU6.Location = new System.Drawing.Point(446, 215);
            this.pictureBoxPSU6.Name = "pictureBoxPSU6";
            this.pictureBoxPSU6.Size = new System.Drawing.Size(20, 20);
            this.pictureBoxPSU6.TabIndex = 28;
            this.pictureBoxPSU6.TabStop = false;
            // 
            // labelMod2
            // 
            this.labelMod2.AutoSize = true;
            this.labelMod2.Location = new System.Drawing.Point(384, 102);
            this.labelMod2.Name = "labelMod2";
            this.labelMod2.Size = new System.Drawing.Size(51, 13);
            this.labelMod2.TabIndex = 29;
            this.labelMod2.Text = "Module 2";
            // 
            // labelMod3
            // 
            this.labelMod3.AutoSize = true;
            this.labelMod3.Location = new System.Drawing.Point(384, 132);
            this.labelMod3.Name = "labelMod3";
            this.labelMod3.Size = new System.Drawing.Size(51, 13);
            this.labelMod3.TabIndex = 30;
            this.labelMod3.Text = "Module 3";
            // 
            // labelMod4
            // 
            this.labelMod4.AutoSize = true;
            this.labelMod4.Location = new System.Drawing.Point(384, 162);
            this.labelMod4.Name = "labelMod4";
            this.labelMod4.Size = new System.Drawing.Size(51, 13);
            this.labelMod4.TabIndex = 31;
            this.labelMod4.Text = "Module 4";
            // 
            // labelMod5
            // 
            this.labelMod5.AutoSize = true;
            this.labelMod5.Location = new System.Drawing.Point(384, 192);
            this.labelMod5.Name = "labelMod5";
            this.labelMod5.Size = new System.Drawing.Size(51, 13);
            this.labelMod5.TabIndex = 32;
            this.labelMod5.Text = "Module 5";
            // 
            // labelMod6
            // 
            this.labelMod6.AutoSize = true;
            this.labelMod6.Location = new System.Drawing.Point(384, 222);
            this.labelMod6.Name = "labelMod6";
            this.labelMod6.Size = new System.Drawing.Size(51, 13);
            this.labelMod6.TabIndex = 33;
            this.labelMod6.Text = "Module 6";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(146, 185);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(206, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "Bus Common voltage             PDU 24VDC";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(509, 218);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "PDU Temp, C";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(509, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "Tamb, C";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.AllowDrop = true;
            this.numericUpDown2.Location = new System.Drawing.Point(213, 65);
            this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(80, 20);
            this.numericUpDown2.TabIndex = 39;
            this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(296, 72);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "samples/cy";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 333);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBoxTamb);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxPDUtemp);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.labelMod6);
            this.Controls.Add(this.labelMod5);
            this.Controls.Add(this.labelMod4);
            this.Controls.Add(this.labelMod3);
            this.Controls.Add(this.labelMod2);
            this.Controls.Add(this.pictureBoxPSU6);
            this.Controls.Add(this.pictureBoxPSU5);
            this.Controls.Add(this.pictureBoxPSU4);
            this.Controls.Add(this.pictureBoxPSU3);
            this.Controls.Add(this.pictureBoxPSU2);
            this.Controls.Add(this.labelMod1);
            this.Controls.Add(this.pictureBoxPSU1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxTemp3);
            this.Controls.Add(this.textBoxTemp2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button_Stop);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxTemp1);
            this.Controls.Add(this.textBox24VDC);
            this.Controls.Add(this.textBox48VDC);
            this.Controls.Add(this.textBoxCycles);
            this.Controls.Add(this.buttonEnd);
            this.Controls.Add(this.buttonStart);
            this.Name = "Form1";
            this.Text = "Bali PSS Tester";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPSU6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonEnd;
        private System.Windows.Forms.TextBox textBoxCycles;
        private System.Windows.Forms.TextBox textBox48VDC;
        private System.Windows.Forms.TextBox textBox24VDC;
        private System.Windows.Forms.TextBox textBoxTemp1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxTemp2;
        private System.Windows.Forms.TextBox textBoxTemp3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.PictureBox pictureBoxPSU1;
        private System.Windows.Forms.Label labelMod1;
        private System.Windows.Forms.PictureBox pictureBoxPSU2;
        private System.Windows.Forms.PictureBox pictureBoxPSU3;
        private System.Windows.Forms.PictureBox pictureBoxPSU4;
        private System.Windows.Forms.PictureBox pictureBoxPSU5;
        private System.Windows.Forms.PictureBox pictureBoxPSU6;
        private System.Windows.Forms.Label labelMod2;
        private System.Windows.Forms.Label labelMod3;
        private System.Windows.Forms.Label labelMod4;
        private System.Windows.Forms.Label labelMod5;
        private System.Windows.Forms.Label labelMod6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxPDUtemp;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxTamb;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label13;
    }
}

