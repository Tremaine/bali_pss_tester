# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Visual Studio 2015 C# project for Bali PSS Tester


### How do I get set up? ###

The test software runs on a Windows platform and interfaces to a test rack using 
   1. Labjack UE9 for SSR control and thermal sensors
   2. Ethernet connection to Prysm PDU
   3. PDU load is three resistive load boxes ( 3000W max possible, not used to that level)

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Brian Tremaine
* 669-273-6035
* btremaine@prysm.com